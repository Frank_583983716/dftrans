@echo off
set starttime=%time%
set currentdisk=%~d0
REM uninstall 旧自定义依赖
pip uninstall psgspecialelements
cd ..\psgspecialelements
python setup.py install --force
cd ..\DFTrans
REM call nuitka --standalone --show-memory --show-progress --enable-plugin=numpy --windows-icon-from-ico=.\config\img\table_arrow9_transparent_64.ico --enable-plugin=pyqt5 --enable-plugin=tk-inter --show-modules --output-dir=d:\temp\myexe  .\DFTrans_main.py
call nuitka --standalone --show-memory --show-progress --windows-disable-console --windows-icon-from-ico=.\config\img\table_arrow9_transparent_64.ico --enable-plugin=pyqt5 --enable-plugin=tk-inter --show-modules --output-dir=%currentdisk%\temp\myexe  .\DFTrans_main.py
REM 拷贝用户定义函数
copy .\df_excel_define_user.py %currentdisk%\temp\myexe\DFTrans_main.dist
xcopy /E/H/C/I/Y .\config %currentdisk%\temp\myexe\DFTrans_main.dist\config
echo starttime: %starttime%
echo endtime:   %time%
