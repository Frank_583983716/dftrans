# -*- coding: utf-8 -*-

import sys
import os
import time

headline = ['#', '##', '###', '####', '#####', '######']
lines_in_file = []


"""生成目录列表中的某一项"""
def creat_directory_line(line, headline_mark, i):
    laylevel = len(headline_mark)
    directorycontent = line[laylevel + 1: -1]
    # 去掉首位空格， 其他空格替换为‘-’， '.'必须去掉， 字母全部小写
    linkmark = '(#' + str.replace(str.replace(directorycontent.rstrip(" "), " ", "-"), ".", "").lower() + ')\n\n'
    totallink = '&emsp;'*(laylevel - 1) + '[' + directorycontent.rstrip(" ") + ']' + linkmark
    return totallink


"""生成目录列表"""
def creat_directory(f):
    i = 0
    directory = []
    directory.append('### 目录\n\n')
    for line in f:
        lines_in_file.append(line)
    f.close()
    length = len(lines_in_file)
    for j in range(length):
        splitedline = lines_in_file[j].lstrip().split(' ')
        if splitedline[0] in headline:
            # 如果为最后一行且末尾无换行（防最后一个字被去除）
            if j == length - 1 and lines_in_file[j][-1] != '\n':
                directory.append(creat_directory_line(lines_in_file[j] + '\n', splitedline[0], i) + '\n')
                # lines_in_file[j] = lines_in_file[j].replace(splitedline[0] + ' ', splitedline[0] + ' ' + '<a name="' + str(i) + '">')[:] + '</a><a style="float:right;text-decoration:none;" href="#index">[Top]</a>' + "\n"
                i = i + 1
            else:
                directory.append(creat_directory_line(lines_in_file[j], splitedline[0], i))
                # lines_in_file[j] = lines_in_file[j].replace(splitedline[0] + ' ', splitedline[0] + ' ' + '<a name="' + str(i) + '">')[:-1] + '</a><a style="float:right;text-decoration:none;" href="#index">[Top]</a>' + "\n"
                i = i + 1
    return directory


"""以目录列表为参数生成添加目录的文件"""
def creat_file_with_toc(file_name, f):
    directory = creat_directory(f)
    filenamesplit = os.path.basename(file_name).split('.')
    resultfilename = ".".join(filenamesplit[:-1]) + "_index." + filenamesplit[-1]
    file_with_toc = os.path.join(os.path.dirname(file_name), resultfilename)
    with open(file_with_toc, 'w+', encoding='utf-8') as f:
        for directory_line in directory:
            f.write(directory_line)
        for line in lines_in_file:
            f.write(line)
        print('文件{}已生成'.format(resultfilename))


if __name__=='__main__':
    file_name = ''
    # 如果未传入文件名
    if len(sys.argv) < 2:
        path = os.getcwd()
        file_and_dir = os.listdir(path)
        md_file = []
        for item in file_and_dir:
            if item.split('.')[-1].lower() in ['md', 'mdown', 'markdown'] and os.path.isfile(item):
                md_file.append(item)
        if len(md_file) != 0:
            print('当前目录下的Markdown文件：')
            for file in md_file:
                print(file)
            file_name = input('请输入文件名(含后缀)\n')
        else:
            print('该目录下无Markdown文件，即将退出...')
            time.sleep(2)
            os._exit(0)
    else:
        file_name = sys.argv[1]
    if os.path.exists(file_name) and os.path.isfile(file_name):
        with open(file_name, 'r', encoding='utf-8') as f:
            creat_file_with_toc(file_name, f)
    else:
        msg = "未找到文件"
        print(msg)
# -*- coding: utf-8 -*-
